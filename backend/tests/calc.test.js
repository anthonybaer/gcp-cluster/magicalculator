const calc = require('../lib/calc.js');

test('Addition Function', () => {
    expect( calc.add(2, 2)).toBe(4)
})

test('Subtract Function', () =>{
    expect(calc.subtract(3, 2)).toBe(1)
})
